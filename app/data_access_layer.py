# Generic Imports
import traceback
import logging

# App Imports
from app import db
from app.models import Attendance, Student, Teacher

# Exceptions Import
from sqlalchemy.exc import InterfaceError, IntegrityError, OperationalError

# Queries Import
from sql_queries import count_attendance_time_for_day_range


class UpdateAttendanceForDate(object):
    def __init__(self, student_id, attendance_event):
        self.student_id = student_id
        self.attendance_event = attendance_event

    def make_attendance_entry(self, attendance_datetime):
        try:
            attendance = Attendance(student_id=self.student_id,
                       date = attendance_datetime.date(),
                       attendance = self.attendance_event)
            db.session.add(attendance)
            db.session.commit()
            saved_data = {"date": str(attendance_datetime.date()),
                          "student_id": self.student_id,
                          "attendance": self.attendance_event
              }
            return True, saved_data
        except InterfaceError as e:
            logging.error("Got exception while creating new member: {}".format(traceback.format_exc()))
            return False, e.message.split(')')[1].strip()

        except IntegrityError as e:
            logging.error("Got Integrity error while creating new member: {}".format(traceback.format_exc()))
            return False, "Attendance already marked for the day."

        except OperationalError as e:
            logging.error("Got Integrity error while creating new member: {}".format(traceback.format_exc()))
            return False, e.message.split(')')[1].strip()

    def make_attendance_entry_for_class(self, attendance_datetime):
        try:
            out = []
            all_students = StudentFromDb().get_all_students()
            for student in all_students:
                status, message = UpdateAttendanceForDate(student, self.attendance_event).make_attendance_entry(attendance_datetime)
                out.append({'status':status, 'data': message})
            return True, out

        except InterfaceError as e:
            logging.error("Got exception while creating new member: {}".format(traceback.format_exc()))
            return False, e.message.split(')')[1].strip()

        except IntegrityError as e:
            logging.error("Got Integrity error while creating new member: {}".format(traceback.format_exc()))
            return False, "Attendance already marked for the day."

        except OperationalError as e:
            logging.error("Got Integrity error while creating new member: {}".format(traceback.format_exc()))
            return False, e.message.split(')')[1].strip()

class StudentFromDb(object):

    def get_student_by_id(self, student_id):
        student = Student.query.filter_by(student_id=self.student_id)
        return student[0].student_id

    def get_all_students(self):
        students = Student.query.all()
        all_students = []
        for student in students:
            all_students.append(student.student_id)
        return all_students

    def insert_student(self, student_id):
        try:
            student = Student(student_id=student_id)
            db.session.add(student)
            db.session.commit()
            saved_data = {"student_id": student_id,
              }
            return True, saved_data

        except InterfaceError as e:
            logging.error("Got exception while creating new member: {}".format(traceback.format_exc()))
            return False, e.message.split(')')[1].strip()

        except IntegrityError as e:
            logging.error("Got Integrity error while creating new member: {}".format(traceback.format_exc()))
            return False, "Student already exists"

        except OperationalError as e:
            logging.error("Got Integrity error while creating new member: {}".format(traceback.format_exc()))
            return False, e.message.split(')')[1].strip()

class GetAttendanceFromDB(object):
    def __init__(self, student_id):
        self.student_id = student_id

    def get_by_date_range(self, start_date, end_date):
        attendance_time = count_attendance_time_for_day_range.format(student_id=self.student_id,
                                                   start_date=str(start_date.date()),
                                                   end_date=str(end_date.date()))

        attendance_result = db.engine.execute(attendance_time)
        attendance = dict()

        for result in attendance_result:
            # date of attendance as key = minutes worked, startime, end_time
                attendance[result[0]] = result[2]

        return attendance

class UserRegister(object):
    def create(self, username, password):
        if Teacher.query.filter_by(username = username).first() is not None:
            return False, {'username': 'already exists'}
        user = Teacher(username = username)
        user.hash_password(password)
        db.session.add(user)
        db.session.commit()
        return True, {'username': user.username}
