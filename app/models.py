from passlib.apps import custom_app_context as pwd_context

# App Imports
from app import db

class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    student_id = db.Column(db.Integer)

    __table_args__ = (db.UniqueConstraint('student_id', name='_student_uc'),)

class Attendance(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    student_id = db.Column(db.Integer, nullable=False)
    date = db.Column(db.Date, nullable=False)
    attendance = db.Column(db.Boolean, nullable=False)

    __table_args__ = (db.UniqueConstraint('student_id', 'date', name='_student_attendance_uc'),
                     )

class Teacher(db.Model):
    __tablename__ = 'teacher'
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(32), index = True)
    password_hash = db.Column(db.String(128))

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)