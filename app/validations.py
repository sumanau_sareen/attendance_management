# Third party library imports

attendance_schema = {'student_id': {'type': 'integer', 'required': True},
 	 	  'date': {'type': 'string', 'required': True, 'regex': '\d{4}[-]\d{2}[-]\d{1,2}'},
 	 	  'attendance': {'type': 'boolean', 'required': True}
		 }

list_attendance_schema = {'start_day': {'type': 'string', 'required': True, 'regex': '\d{4}[-]\d{2}[-]\d{1,2}'},
				 	 	  'end_day': {'type': 'string', 'required': True, 'regex': '\d{4}[-]\d{2}[-]\d{1,2}'},
						  'student_id': {'type': 'string', 'required': True}
						} 

class_attendance_schema = {'date': {'type': 'string', 'required': True, 'regex': '\d{4}[-]\d{2}[-]\d{1,2}'},
 	 	  'attendance': {'type': 'boolean', 'required': True}
 	 	  }

user_schema = {'username': {'type': 'string', 'required': True},
					   'password': {'type': 'string', 'required': True}
					   }