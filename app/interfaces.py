import logging

from data_access_layer import UpdateAttendanceForDate, \
                              GetAttendanceFromDB, \
                              StudentFromDb

class AttendanceRegister(object):
    def __init__(self, student_id):
        self.student_id = student_id

    def record_attendance(self, event_date, attendance_event):
        logging.info("Calling DB class to mark attendance")
        StudentFromDb().insert_student(self.student_id)
        update_attendance = UpdateAttendanceForDate(self.student_id, attendance_event)
        status, data = update_attendance.make_attendance_entry(event_date)
        return status, data

    def record_class_attendance(self, event_date, attendance_event):
        logging.info("Calling DB class to mark class attendance")
        update_attendance = UpdateAttendanceForDate(self.student_id, attendance_event)
        status, data = update_attendance.make_attendance_entry_for_class(event_date)
        return status, data


class ListAttendance(object):
    def __init__(self, student_id):
        self.student_id = student_id

    def get_range(self, start_date, end_date):
        logging.info("Calling DB class to retrieve attendance data")
        attendance_result = GetAttendanceFromDB(
                                                self.student_id
                                                ).get_by_date_range(
                                                            start_date,
                                                            end_date)
        if not attendance_result:
            return False, 'No Attendance data'

        output = []
        for date, attendance_metadata in attendance_result.iteritems():
            data = {"date": date,
                    "present": attendance_metadata
                    }
            output.append(data)

        return True, output
