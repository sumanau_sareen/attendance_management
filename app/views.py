# Standard Library Imports
import logging
from datetime import datetime

# Flask Imports
from flask.views import MethodView
from flask import request
from flask_httpauth import HTTPBasicAuth
auth = HTTPBasicAuth()

from app.models import Teacher
from flask import g

# Project Imports
from interfaces import AttendanceRegister, ListAttendance
from data_access_layer import UserRegister
from response import AttendanceSuccessResponse, \
                     ListAttendanceSuccessResponse, ClientErrorResponse, UserSuccessResponse
from validations import attendance_schema, list_attendance_schema, class_attendance_schema, \
                        user_schema

# Third-party Library imports
from cerberus import Validator

@auth.verify_password
def verify_password(username, password):
    teacher = Teacher.query.filter_by(username = username).first()
    if not teacher or not teacher.verify_password(password):
        return False
    g.user = teacher
    return True

class UserAPI(MethodView):
    def post(self):
        data = request.get_json()
        logging.info("POST: Check if input data is valid {}".format(data))
        # Check if data is valid or not
        user_data_validator = Validator()
        valid = user_data_validator.validate(data, user_schema)
        
        if not valid:
            logging.warning("POST: Invalid data received from client {}".format(data))
            # Send error message to client
            error =  user_data_validator.errors
            message = ''
            for field_name, error_message in error.iteritems():
                message += '{} {}\n'.format(field_name, error_message)
            return ClientErrorResponse(message)

        logging.info("POST: Input data valid for creating user")
        username = data.pop('username')
        password = data.pop('password')
        logging.info("POST: Creating teacher with username {}".format(username))

        status, user_info = UserRegister().create(username, password)
        if status:
            logging.info("POST: Successfully marked attendance.")
            response = UserSuccessResponse(user_info)
            return response
        else:
            logging.warning("POST: User not created.")
            response = ClientErrorResponse(user_info)
            return response


class AttendanceAPI(MethodView):
    ''' API Service layer to handle incoming requests 
        to handle events for marking attendance for employees
        handled by POST request.
        And for listing attendance handled by GET request.
    '''
    decorators = [auth.login_required]
    def get(self, id=None):
        data = request.args
        # Check if data is valid or not
        logging.info("GET: Check if input data is valid {}".format(data))
        list_attendance_data_validator = Validator()
        valid = list_attendance_data_validator.validate(data, list_attendance_schema)
        
        if not valid:
            logging.warning("GET: Invalid data received from client {}".format(data))
            # Send error message to client
            error =  list_attendance_data_validator.errors
            message = ''
            for field_name, error_message in error.iteritems():
                message += '{} {}\n'.format(field_name, error_message)
            return ClientErrorResponse(message)

        # As request params are str, convert id to int
        logging.info("GET: Input data valid for list attendance")
        student_id = int(data.get('student_id'))
        start_date = datetime.strptime(data.get('start_day'), '%Y-%m-%d')
        end_date = datetime.strptime(data.get('end_day'), '%Y-%m-%d')
        logging.info("GET: Calling BLogic to get employee attendance data")
        success, list_attendance_info = ListAttendance(student_id).get_range(start_date, end_date)
        if success:
            logging.info("GET: Successfully received attendance data.")
            response = ListAttendanceSuccessResponse(list_attendance_info)
        else:
            logging.warning("GET: Invalid response.")
            response = ClientErrorResponse(list_attendance_info)
        return response

    def post(self):
        data = request.get_json()
        logging.info("POST: Check if input data is valid {}".format(data))
        # Check if data is valid or not
        attendance_data_validator = Validator()
        valid = attendance_data_validator.validate(data, attendance_schema)
        
        if not valid:
            logging.warning("POST: Invalid data received from client {}".format(data))
            # Send error message to client
            error =  attendance_data_validator.errors
            message = ''
            for field_name, error_message in error.iteritems():
                message += '{} {}\n'.format(field_name, error_message)
            return ClientErrorResponse(message)

        logging.info("POST: Input data valid for Attendance event")
        attendance_event = data.pop('attendance')
        student_id = data.pop('student_id')
        attendance_date_str = '{}'.format(data.pop('date'))
        attendance_date = datetime.strptime(attendance_date_str, '%Y-%m-%d')
        logging.info("POST: Marking attendance for student {} with event {}".format(student_id,attendance_event))
        status, attendance_info = AttendanceRegister(student_id).record_attendance(attendance_date, attendance_event)
        if status:
            logging.info("POST: Successfully marked attendance.")
            response = AttendanceSuccessResponse(attendance_info, attendance_event)
            return response
        else:
            logging.warning("POST: Attendance not marked.")
            response = ClientErrorResponse(attendance_info)
            return response

class ClassAttendanceAPI(MethodView):
    ''' API Service layer to handle incoming requests 
        to handle events for marking attendance for employees
        handled by POST request.
        And for listing attendance handled by GET request.
    '''
    decorators = [auth.login_required]
    def post(self, id=None):
        data = request.get_json()
        logging.info("POST: Check if input data is valid {}".format(data))
        # Check if data is valid or not
        class_attendance_data_validator = Validator()
        valid = class_attendance_data_validator.validate(data, class_attendance_schema)
        
        if not valid:
            logging.warning("GET: Invalid data received from client {}".format(data))
            # Send error message to client
            error =  class_attendance_data_validator.errors
            message = ''
            for field_name, error_message in error.iteritems():
                message += '{} {}\n'.format(field_name, error_message)
            return ClientErrorResponse(message)

        # As request params are str, convert id to int
        logging.info("POST: Input data valid Class for Attendance event")
        attendance_event = data.pop('attendance')
        attendance_date_str = '{}'.format(data.pop('date'))
        attendance_date = datetime.strptime(attendance_date_str, '%Y-%m-%d')
        logging.info("POST: Marking attendance for class with event {}".format(attendance_event))
        status, attendance_info = AttendanceRegister(student_id=None).record_class_attendance(attendance_date, attendance_event)
        if status:
            logging.info("POST: Successfully marked attendance.")
            response = AttendanceSuccessResponse(attendance_info, attendance_event)
            return response
        else:
            logging.warning("POST: Attendance not marked.")
            response = ClientErrorResponse(attendance_info)
            return response
