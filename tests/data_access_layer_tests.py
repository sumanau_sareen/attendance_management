# Standard Library Imports
import unittest
import datetime
import os
import traceback

basedir = os.path.abspath(os.path.dirname(__file__))

# Project Imports
from app import app, db

# Classes to test Imports
from app.data_access_layer import GetAttendanceFromDB, \
                                    UpdateAttendanceForDate, StudentFromDb

class TestAttendance(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'test1.db')
        self.app = app.test_client()
        db.create_all()

    def step1_test_set_attendance_true(self):
        student_id = 1
        date = datetime.datetime.now()
        attendance = True
        expected_student_status = True
        expected_student_data = {'student_id': student_id}
        student_status, student_data = StudentFromDb().insert_student(student_id)
        expected_status = True
        expected_data = {'date': '2017-10-13', 'student_id': 1, 'attendance': True}
        status, data = UpdateAttendanceForDate(student_id, attendance).make_attendance_entry(date)
        self.assertEqual(expected_student_status, student_status)
        self.assertEqual(expected_student_data, student_data)
        print '**** Student successfully created. **** \n'
        self.assertEqual(status,expected_status)
        self.assertEqual(data, expected_data)
        print '**** Attendance marked successfully for student ****\n'

    def step2_test_get_attendance_by_date_range_per_student(self):
        student_id = 1
        start_date = end_date = datetime.datetime.now()
        expected_attendance = {'2017-10-13': 1}
        attendance = GetAttendanceFromDB(student_id).get_by_date_range(start_date, end_date)
        self.assertEqual(attendance,expected_attendance)
        print '**** Attendance retrieved successfully ****\n'

    def step3_test_mark_class_attendance(self):
        date = datetime.datetime.now() + datetime.timedelta(days=1)
        expected_attendance_response = [{'status': True, 'data': {'date': '2017-10-14', 'student_id': 1, 'attendance': True}}]
        attendance = True
        status, attendance_response = UpdateAttendanceForDate(None, attendance).make_attendance_entry_for_class(date)
        self.assertEqual(attendance_response, expected_attendance_response)
        print '**** Class attenadance marked successfully ***\n'

    def test_attendance_flow(self):
        try:
            self.step1_test_set_attendance_true()
            self.step2_test_get_attendance_by_date_range_per_student()
            self.step3_test_mark_class_attendance()
        except:
            print traceback.format_exc()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        print 'Tear Down Complete'

if __name__ == '__main__':
    unittest.main()