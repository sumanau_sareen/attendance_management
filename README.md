# Attendance Management
Attendance Management minimalist framework

## Tech Stack
Development Language: Python
Web Framework: Flask
Database ORM: SQLAlchemy
Database: SQLITE

1. Create a virtualenv using virtualenvwrapper: `mkvirtualenv att_management`
2. Install requirements: `pip install -r requirements.txt`
3. Create database: `python db_create.py`
4. Create Migration: `python db_migrate.py`
5. `python run.py` - For local testing

# Routes Expossed: </br>
## User Creation
`POST` /user/ - Creates a user (teacher). User is authorised to mark attendance.
## Attendance:</br>
### Needs Basic HTTPAuthentication username:password
`POST` /attendance/ - Marks attendance for the given day of a student. </br>
`GET` /attendance/ - Listing attendance by date range for a student </br>
`POST` /attendance/class - Marks attendance for the class for a given date </br>
